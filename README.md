# tempo

[![Crates.io](https://img.shields.io/crates/v/tempo.svg?style=flat-square)](https://crates.io/crates/tempo)

[Documentation](https://doc.robigalia.org/tempo)

Sane timekeeping.

## Status

In development.
