// Copyright (c) 2015 The Robigalia Project Developers Licensed under the Apache License, Version
// 2.0 <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option. All files in the project
// carrying such notice may not be copied, modified, or distributed except according to those
// terms.

#[thread_local]
static mut GLOBAL_DATA: *mut u8 = 0 as *mut u8;
#[thread_local]
static mut THREAD_DATA: *mut u8 = 0 as *mut u8;

use core::sync::atomic::{AtomicU32, Ordering};
use core::ops::Range;

use {Tick, TickDifference, Tai, TaiDifference, StartKind, DiscontinuityKind};

#[inline(always)]
/// A monotonically non-decreasing counter. Increases at a steady rate,
/// currently estimated by `get_ratio`.
///
/// Note that both monotonicity and steadiness are properties of a hardware
/// clock. When migrating across virtual machine hosts or other boundaries,
/// timing weirdness may be observed. By ignoring any ticks inside a
/// discontinuity range, and keeping track of `clocksource_id`, the worst can be
/// avoided.
pub fn get_now() -> Tick {
    if cfg!(target_pointer_width = "64") {
        let lo: u64;
        let hi: u64;
        unsafe {
            asm!("rdtscp" : "={rax}"(lo), "={rdx}"(hi) : : "%rcx" : "volatile");
        }
        Tick(lo | (hi << 32))
    } else {
        let lo: u32;
        let hi: u32;
        unsafe {
            asm!("cpuid\nrdtsc" : "={eax}"(lo), "={edx}"(hi) : : "%ebx", "%ecx" : "volatile");
        }
        Tick(lo as u64 | ((hi as u64) << 32))
    }
}

fn i128_from_parts(hi: u64, lo: u64) -> i128 {
    ((hi as u128) << 64 | (lo as u128)) as i128
}

unsafe fn decode_discontinuity(ptr: &RawDiscontinuity)
                               -> (DiscontinuityKind, Range<Tick>, TaiDifference) {
    let discont = match ptr.discrim {
        0 => DiscontinuityKind::Suspend,
        1 => DiscontinuityKind::Wraparound,
        _ => unreachable!(),
    };
    (discont,
     (Tick(ptr.range_lo)..Tick(ptr.range_hi)),
     TaiDifference { diff: i128_from_parts(ptr.dur_hi, ptr.dur_lo) })
}

#[repr(C)]
struct RawDiscontinuity {
    discrim: u8,
    range_lo: u64,
    range_hi: u64,
    dur_lo: u64,
    dur_hi: u64,
}

/// Get any discontinuities that occurred since some start point.
///
/// The OS does not maintain the tick list forever. In particular, the buffer of
/// discontinuities has a fixed size. Through mechanisms not described here, threads
/// can register to receive a notification when the buffer is about to fill, and copy
/// out all the remaining discontinuities for storage.
///
/// A discontinuity is some period of time where the counter wasn't increasing.
/// Due to difficulties in determining exactly how long and when the system
/// went up/down, the `TaiDifference` portion of the tuple is merely an estimate.
/// The `Range<Tick>` portion has no relation to the actual tick rate, and
/// merely indicates a convenient place where the count stopped/started.
///
/// The usize this returns is the number of discontinuities that couldn't fit
/// into the slice, and were dropped. There can only ever be at most 120 recent
/// discontinuities, so passing a slice of length 120 will always avoid this
/// issue.
pub fn get_recent_discontinuities(start: Tick,
                                  read_into: &mut [(DiscontinuityKind,
                                                    Range<Tick>,
                                                    TaiDifference)])
                                  -> usize {
    unsafe {
        let pg = GLOBAL_DATA;
        let discont_off = *((pg as *mut u16).offset(2)) as isize;
        let discont_base = pg.offset(discont_off).offset(22) as *mut RawDiscontinuity;
        let discont_len = *(pg.offset(discont_off).offset(4) as *mut u16) as usize;
        let discont_seqno = pg.offset(discont_off) as *mut AtomicU32;
        let disconts = ::core::slice::from_raw_parts(discont_base as *const RawDiscontinuity,
                                                     discont_len);
        let mut len;
        loop {
            let seqno = (&*discont_seqno).load(Ordering::Acquire);

            let start = match disconts.binary_search_by(|raw| start.0.cmp(&raw.range_lo)) {
                Ok(val) => val,
                Err(val) => val,
            };
            len = discont_len - start;
            let actual_len = ::core::cmp::min(len, read_into.len());
            for (idx, val) in disconts.iter().skip(start).take(actual_len).enumerate() {
                read_into[idx] = decode_discontinuity(val);
            }
            if seqno & 1 == 0 && (&*discont_seqno).load(Ordering::Acquire) == seqno {
                break;
            }
        }
        len
    }
}

/// Get the total number of discontinuities available.
///
/// This is generally going to be less than 120.
pub fn get_discontinuity_count() -> usize {
    unsafe {
        let pg = GLOBAL_DATA;
        let discont_off = *((pg as *mut u16).offset(2)) as isize;
        *((pg.offset(discont_off) as *mut u16).offset(3)) as usize
    }
}

/// Get the total (estimated) amount of time the system has been suspended.
///
/// This is a running sum of all `TaiDifference`s reported in a discontinuity
/// with DiscontinuityKind::Suspend. This is offered as a convenience. It is
/// possible to compute this in userspace, but it is somewhat annoying.
pub fn get_suspend_time() -> TaiDifference {
    unsafe {
        let pg = GLOBAL_DATA;
        let discont_off = *((pg as *mut u16).offset(2)) as isize;
        let tot_ptr = pg.offset(discont_off + 6) as *mut u64;
        let discont_seqno = pg.offset(discont_off) as *mut AtomicU32;
        let tot;
        loop {
            let seqno = (&*discont_seqno).load(Ordering::Acquire);
            let tot_lo = *tot_ptr;
            let tot_hi = *tot_ptr.offset(1);
            if seqno & 1 == 0 && (&*discont_seqno).load(Ordering::Acquire) == seqno {
                tot = TaiDifference { diff: i128_from_parts(tot_hi, tot_lo) };
                break;
            }
        }
        tot
    }
}

/// Get the number of ticks that pass per some number of nanoseconds.
///
/// This is, necessarily, an estimation.
pub fn get_ratio() -> (TickDifference, TaiDifference) {
    unsafe {
        let pg = GLOBAL_DATA;
        let ratio_off = *((pg as *mut u16).offset(1)) as isize;
        let ratio_seqno = (pg.offset(ratio_off) as *mut u32).offset(8 + 16) as *mut AtomicU32;
        let mut ratio_high: u64;
        let mut ratio_low_low: u64;
        let mut ratio_low_high: u64;
        loop {
            let seqno = (&*ratio_seqno).load(Ordering::Acquire);
            ratio_high = *(pg.offset(ratio_off) as *mut u64);
            ratio_low_low = *((pg.offset(ratio_off) as *mut u64).offset(1));
            ratio_low_high = *((pg.offset(ratio_off) as *mut u64).offset(2));

            if seqno & 1 == 0 && ((&*ratio_seqno).load(Ordering::Acquire) == seqno) {
                break;
            }
        }
        (TickDifference { diff: ratio_high as i64 },
         TaiDifference { diff: i128_from_parts(ratio_low_high, ratio_low_low) })
    }
}

/// Get the wall clock time the system was booted at.
///
/// This is, necessarily, an estimate, usually achieved by synchronizing with
/// some number of network time servers, which themselves have either a
/// reference clock (GPS or atomic, usually) or are themselves synchronized with
/// some network time server.
pub fn get_wall_boot() -> Tai {
    unsafe {
        let pg = GLOBAL_DATA;
        let wb_off = *(pg as *mut u16) as isize;
        let wb_seqno = (pg.offset(wb_off) as *mut u32).offset(16) as *mut AtomicU32;
        let mut wb_high: u64;
        let mut wb_low: u64;
        loop {
            let seqno = (&*wb_seqno).load(Ordering::Acquire);
            wb_low = *((pg.offset(wb_off) as *mut u64));
            wb_high = *((pg.offset(wb_off) as *mut u64).offset(1));

            if seqno & 1 == 0 && ((&*wb_seqno).load(Ordering::Acquire) == seqno) {
                break;
            }
        }
        Tai(i128_from_parts(wb_high, wb_low))
    }
}

/// Get the start time, in ticks, of the thread, process, or machine.
pub fn get_start(arg: StartKind) -> Tick {
    unsafe {
        match arg {
            StartKind::Machine => Tick(*((GLOBAL_DATA as *mut u16).offset(3) as *mut u64)),
            StartKind::Process => Tick(*(THREAD_DATA as *mut u64).offset(4)),
            StartKind::Thread => Tick(*(THREAD_DATA as *mut u64).offset(3)),
            StartKind::__Nonexhaustive => unreachable!(),
        }
    }
}

/// Get an ID of the current clocksource that `get_now` measures.
///
/// There can be a single clock per NUMA node, per socket, per core, etc.
pub fn get_clocksource_id() -> u64 {
    unsafe { *(THREAD_DATA as *mut u64) }
}

pub unsafe fn set_addresses(global: *mut u8, thread: *mut u8) {
    GLOBAL_DATA = global;
    THREAD_DATA = thread;
}
