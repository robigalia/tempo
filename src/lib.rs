// Copyright (c) 2015 The Robigalia Project Developers Licensed under the Apache License, Version
// 2.0 <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your option. All files in the project
// carrying such notice may not be copied, modified, or distributed except according to those
// terms.

#![no_std]
#![feature(asm, thread_local, integer_atomics, i128_type)]

use core::ops::{Add, Mul, Div, Sub};

// {{{ Types

/// A timestamp in International Atomic Time, with nanosecond resolution.
///
/// This is taken relative to the Unix epoch, 1970-01-01T00:00:00Z.
///
/// It is represented with a signed 128-bit integer, and can represent times approximately
/// ±5 zettayears from the Unix epoch. This should be sufficient for all practical purposes,
/// measuring far before the beginning of the observable universe and far past the estimated time
/// at which all stars in the universe will have burned out.
///
/// This is not a very practical type for most purposes. Instead, prefer something like the
/// `chrono` crate for dealing with times outside of the very limited manipulation needed with the
/// return type of `get_ratio` or `get_boot_time`.
#[derive(PartialOrd, Ord, PartialEq, Eq, Copy, Clone)]
pub struct Tai(i128);

impl Tai {
    pub fn from_ns(ns: i128) -> Tai {
        Tai(ns)
    }

    pub fn to_ns(&self) -> i128 {
        self.0
    }
}

/// Offset between two `Tai` values.
///
/// This represents a duration.
#[derive(PartialOrd, Ord, PartialEq, Eq, Copy, Clone)]
pub struct TaiDifference {
    diff: i128,
}

impl TaiDifference {
    pub fn to_ns_delta(&self) -> i128 {
        self.diff
    }
}

/// A timestamp in unspecified units since some unspecified point in time.
///
/// In practice, these will always be relative to `get_boot_time()`. The number
/// of ticks per unit of real time can be retrieved with `get_ratio()`.
#[derive(PartialOrd, Ord, PartialEq, Eq, Copy, Clone)]
pub struct Tick(u64);

impl Tick {
    /// Construct a `Tick` from a raw tick count.
    pub fn from_ticks(val: u64) -> Tick {
        Tick(val)
    }

    /// Fetch the raw tick count.
    pub fn to_ticks(&self) -> u64 {
        self.0
    }
}

/// Offset between two `Tick` values.
///
/// This represents a duration.
#[derive(PartialOrd, Ord, PartialEq, Eq, Copy, Clone)]
pub struct TickDifference {
    diff: i64,
}

/// The source of a discontinuity.
#[derive(PartialOrd, Ord, PartialEq, Eq, Copy, Clone)]
pub enum DiscontinuityKind {
    /// The discontinuity arose due to the machine being in a low-power state
    /// where the tick counter was not maintained.
    Suspend,
    /// The discontinuity arose due to the tick counter wrapping around, due to
    /// size limitations in the system clock.
    Wraparound,
    #[doc(hidden)]
    __Nonexhaustive,
}

/// Various sources of useful epochs for measuring time from.
#[derive(PartialOrd, Ord, PartialEq, Eq, Copy, Clone)]
pub enum StartKind {
    /// Time of system boot where the tick counter was first started from.
    Machine,
    /// Time of process creation.
    Process,
    /// Time of thread creation.
    Thread,
    #[doc(hidden)]
    __Nonexhaustive,
}

// }}}

// {{{ Operator overloads

/// Note: By default, Tick arithmetic is saturating
impl Sub for Tick {
    type Output = TickDifference;

    #[inline]
    fn sub(self, other: Self) -> Self::Output {
        let raw_diff = self.0.wrapping_sub(other.0) as i64;
        let diff = if self.0 > other.0 && raw_diff < 0 {
            ::core::i64::MAX
        } else if self.0 < other.0 && raw_diff > 0 {
            ::core::i64::MIN
        } else {
            raw_diff
        };
        TickDifference { diff: diff }
    }
}

impl Tick {
    /// Try to subtract two ticks.
    ///
    /// This can fail if trying to create a difference which isn't representable
    /// by `TickDifference`, which is signed.
    pub fn checked_sub(&self, other: Tick) -> Option<TickDifference> {
        let raw_diff = self.0.wrapping_sub(other.0) as i64;
        if self.0 > other.0 && raw_diff < 0 {
            None
        } else if self.0 < other.0 && raw_diff > 0 {
            None
        } else {
            Some(TickDifference { diff: raw_diff })
        }
    }

    /// Try to apply an offset to this tick.
    ///
    /// This can fail if trying to create a negative tick, or if trying to
    /// create a tick larger than the maximum representable value.
    pub fn try_offset(&self, other: TickDifference) -> Option<Tick> {
        self.0.checked_add(other.diff as u64).map(Tick)
    }
}

//impl Add<Tick> for TickDifference {
//    type Output = Tick;
//
//    #[inline]
//    fn add(self, other: Tick) -> Self::Output {
//        Tick(self.diff.saturating_add(other.0 as i64) as u64)
//    }
//}
//
//impl Add<TickDifference> for Tick {
//    type Output = Tick;
//
//    #[inline]
//    fn add(self, other: TickDifference) -> Self::Output {
//        Tick(other.diff.saturating_add(self.0 as i64) as u64)
//    }
//}
//
//impl Sub<TickDifference> for Tick {
//    type Output = Tick;
//
//    #[inline]
//    fn sub(self, other: TickDifference) -> Self::Output {
//        Tick(self.0.saturating_sub(other.diff as u64))
//    }
//}

/// Note: By default, TickDifference arithmetic is saturating
impl Add for TickDifference {
    type Output = TickDifference;

    #[inline]
    fn add(self, other: TickDifference) -> Self::Output {
        TickDifference { diff: self.diff.saturating_add(other.diff) }
    }
}

/// Note: By default, TickDifference arithmetic is saturating
impl Sub for TickDifference {
    type Output = TickDifference;

    #[inline]
    fn sub(self, other: TickDifference) -> Self::Output {
        TickDifference { diff: self.diff.saturating_sub(other.diff) }
    }
}

impl TickDifference {
    pub fn raw_diff(&self) -> i64 {
        self.diff
    }

    pub fn checked_add(self, other: TickDifference) -> Option<TickDifference> {
        self.diff.checked_add(other.diff)
            .map(|diff| TickDifference { diff: diff })
    }

    pub fn checked_sub(self, other: TickDifference) -> Option<TickDifference> {
        self.diff.checked_sub(other.diff)
            .map(|diff| TickDifference { diff: diff })
    }
}

macro_rules! tick_diff_mul_impl {
    ($t:ident, $u:ident) => {
        impl Mul<$u> for $t {
            type Output = $t;

            #[inline]
            fn mul(self, other: $u) -> Self::Output {
                $t { diff: self.diff.wrapping_mul(other as i64) }
            }
        }
    }
}

macro_rules! tick_diff_div_impl {
    ($t:ident, $u:ident) => {
        impl Div<$u> for $t {
            type Output = $t;

            #[inline]
            fn div(self, other: $u) -> Self::Output {
                $t { diff: self.diff / (other as i64) }
            }
        }
    }
}

tick_diff_mul_impl!(TickDifference, u8);
tick_diff_mul_impl!(TickDifference, u16);
tick_diff_mul_impl!(TickDifference, u32);
tick_diff_mul_impl!(TickDifference, u64);

tick_diff_div_impl!(TickDifference, u8);
tick_diff_div_impl!(TickDifference, u16);
tick_diff_div_impl!(TickDifference, u32);
tick_diff_div_impl!(TickDifference, u64);

/// Note: By default, Tai arithmetic is saturating
impl Sub for Tai {
    type Output = TaiDifference;

    #[inline]
    fn sub(self, other: Self) -> Self::Output {
        TaiDifference { diff: self.0.saturating_sub(other.0) }
    }
}

impl Tai {
    /// Try to subtract two Tais.
    ///
    /// This can fail if trying to create a difference which isn't representable
    /// by `TaiDifference`, which is signed.
    pub fn checked_sub(&self, other: Tai) -> Option<TaiDifference> {
        self.0.checked_sub(other.0).map(|x| TaiDifference { diff: x })
    }

    /// Try to apply an offset to this Tai.
    ///
    /// This can fail if trying to create a negative tick, or if trying to
    /// create a tick larger than the maximum representable value.
    pub fn try_offset(&self, other: TaiDifference) -> Option<Tai> {
        self.0.checked_add(other.diff).map(Tai)
    }
}

/// Note: By default, Tai arithmetic is saturating
impl Add<TaiDifference> for Tai {
    type Output = Tai;

    #[inline]
    fn add(self, other: TaiDifference) -> Tai {
        Tai(self.0.saturating_add(other.diff))
    }
}

/// Note: By default, Tai arithmetic is saturating
impl Add<Tai> for TaiDifference {
    type Output = Tai;

    #[inline]
    fn add(self, other: Tai) -> Self::Output {
        Tai(self.diff.saturating_add(other.0))
    }
}

/// Note: By default, Tai arithmetic is saturating
impl Sub<TaiDifference> for Tai {
    type Output = Tai;

    #[inline]
    fn sub(self, other: TaiDifference) -> Tai {
        Tai(self.0.saturating_sub(other.diff))
    }
}

/// Note: By default, TaiDifference arithmetic is saturating
impl Add for TaiDifference {
    type Output = TaiDifference;

    #[inline]
    fn add(self, other: TaiDifference) -> Self::Output {
        TaiDifference { diff: self.diff.saturating_add(other.diff) }
    }
}

/// Note: By default, TaiDifference arithmetic is saturating
impl Sub for TaiDifference {
    type Output = TaiDifference;

    #[inline]
    fn sub(self, other: TaiDifference) -> Self::Output {
        TaiDifference { diff: self.diff.saturating_sub(other.diff) }
    }
}

impl TaiDifference {
    fn checked_add(self, other: TaiDifference) -> Option<TaiDifference> {
        self.diff.checked_add(other.diff)
            .map(|diff| TaiDifference { diff: diff })
    }

    fn checked_sub(self, other: TaiDifference) -> Option<TaiDifference> {
        self.diff.checked_sub(other.diff)
            .map(|diff| TaiDifference { diff: diff })
    }
}

macro_rules! tai_diff_mul_impl {
    ($t:ident, $u:ident) => {
        impl Mul<$u> for $t {
            type Output = $t;

            #[inline]
            fn mul(self, other: $u) -> Self::Output {
                $t { diff: self.diff.wrapping_mul(other as i128) }
            }
        }
    }
}

macro_rules! tai_diff_div_impl {
    ($t:ident, $u:ident) => {
        impl Div<$u> for $t {
            type Output = $t;

            #[inline]
            fn div(self, other: $u) -> Self::Output {
                $t { diff: self.diff / (other as i128) }
            }
        }
    }
}

tai_diff_mul_impl!(TaiDifference, u8);
tai_diff_mul_impl!(TaiDifference, u16);
tai_diff_mul_impl!(TaiDifference, u32);
tai_diff_mul_impl!(TaiDifference, u64);

tai_diff_div_impl!(TaiDifference, u8);
tai_diff_div_impl!(TaiDifference, u16);
tai_diff_div_impl!(TaiDifference, u32);
tai_diff_div_impl!(TaiDifference, u64);

impl Div<(TickDifference, TaiDifference)> for TickDifference {
    type Output = TaiDifference;

    fn div(self, other: (TickDifference, TaiDifference)) -> Self::Output {
        // (self * 1) / 0
        if let Some(no_overflow) = other.1.diff.checked_mul(self.diff as i128) {
            TaiDifference { diff: no_overflow }
        } else {
            // Should we saturate or is the division correct?
            unimplemented!()
        }
    }
}

fn real_muldiv(a: i128, b: i64, c: i128) -> Option<i64> {
    let splitval: i128 = 1 << 64;
    let i64min = core::i64::MIN as i128;
    let i64max = core::i64::MAX as i128;

    // Take the high and low halves of a
    let (a_hi, a_lo) = (a / splitval, a % splitval);
    // Multiply them by b
    let b_ = b as i128;
    let (top, bot) = (a_hi * b_, a_lo * b_);
    let (div, rem, div2) = (top / c, top % c, bot / c);
    if div2 >= i64min && div2 <= i64max {
        if div == 0 && rem == 0 {
            // If our return value is in range, produce it
            Some((div2 & 0xFFFFFFFFFFFFFFFF) as i64)
        } else if div == 0 {
            // If our upper limb produced a remainder, fold it in and check it
            real_muldiv(splitval, unimplemented!(), c).and_then(|q| {
                let w = div2 + (q as i128);
                // If after folding we're still in range, we're done
                if w >= i64min && w <= i64max {
                    Some((w & 0xFFFFFFFFFFFFFFFF) as i64)
                } else {
                    None
                }
            })
        } else {
            // If our upper limb had a value after division, or our lower limb is out of range,
            // the result is an overflow (or negative overflow)
            None
        }
    } else {
        None
    }
}

impl Mul<(TickDifference, TaiDifference)> for TaiDifference {
    type Output = TickDifference;
    fn mul(self, other: (TickDifference, TaiDifference)) -> Self::Output {
        TickDifference { diff: 0 }
    }
}

// }}}

//#[cfg(target_arch = "x86")]
mod arch {
    include!("arch/x86.rs");
}

#[cfg(all(target_arch = "arm", target_pointer_width = "32"))]
mod arch {
    include!("arch/arm.rs");
}

pub use arch::*;

#[cfg(test)]
mod tests {
    use ::*;

    // These could be examples, but until we're self-hosting or have a Linux
    // implementation or something, there's not much value to it. Just make
    // sure they compile
    #[allow(dead_code)]
    fn bench<F: FnOnce()>(f: F) -> TaiDifference {
        let start = get_now();
        f();
        let stop = get_now();

        let time = stop - start;
        let rate = get_ratio();
        let nanos = time / rate;
        nanos
    }

    enum RaceWinner {
        A,
        B,
        Tie
    }

    #[allow(dead_code)]
    fn race<A: Fn(), B: Fn()>(a: A, b: B) -> RaceWinner {
        let start = get_now();
        a();
        let mid = get_now();
        b();
        let stop = get_now();

        let a_time = mid - start;
        let b_time = stop - mid;
        if a_time < b_time {
            RaceWinner::A
        } else if a_time > b_time {
            RaceWinner::B
        } else {
            RaceWinner::Tie
        }
    }

    #[allow(dead_code)]
    fn uptime() -> TaiDifference {
        let now = get_now();
        let boot = get_start(StartKind::Machine);
        let stopped_nanos = get_suspend_time();
        let total_time = now - boot;
        (total_time / get_ratio()) - stopped_nanos
    }

    #[allow(dead_code)]
    fn realtime() -> Tai {
        let now = get_now();
        let boot = get_start(StartKind::Machine);
        let stopped_nanos = get_suspend_time();
        let total_time = now - boot;
        get_wall_boot() + (total_time / get_ratio()) + stopped_nanos
    }
}
